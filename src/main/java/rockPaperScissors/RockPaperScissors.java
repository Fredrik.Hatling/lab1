package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while (true) {
            //Print the new round message
            System.out.println("Let's play round " + roundCounter);

            //Generate random computer coise
            Random computerCoice = new Random();
            String random = rpsChoices.get(computerCoice.nextInt(rpsChoices.size()));

            //Tekes input from the human
            String humanMove = readInput("Your choice (Rock/Paper/Scissors)?");

            while (rpsChoices.contains(humanMove) != true) {
                humanMove = readInput("I do not understand " + humanMove + ". Could you try again?");
            }
            
            if (humanMove.equals("paper") && random.equals("rock")
            || humanMove.equals("rock") && random.equals("scissors")
            || humanMove.equals("scissors") && random.equals("paper")) {
                humanScore += 1;
            }
            
            else if (humanMove.equals("rock") && random.equals("paper")
            || humanMove.equals("scissors") && random.equals("rock")
            || humanMove.equals("paper") && random.equals("scissors")) {
                computerScore += 1;
            }

            System.out.print("Human chose " + humanMove + ", computer chose " + random + ". ");
            System.out.println(winLose(humanMove, random));

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String fortsette = readInput("Do you wish to continue playing? (y/n)?");

            roundCounter += 1;

            if (fortsette.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            

        }

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    public String winLose(String user, String computer) {
        String computerWins = "Computer wins!";
        String HumanWins = "Human wins!";
        String tie = "It's a tie!";

        if (computer.equals(user)) {
            return tie;
        }

        if (user.equals("rock")) {
            if (computer.equals("scissors")) {
                return HumanWins;
            }
           else {
               return computerWins;
           } 
        }
        
        else if (user.equals("paper")) {
            if (computer.equals("scissors")) {
                return computerWins;
            }
            else {
                return HumanWins;
            }
        }

        else {
            if (computer.equals("paper")) {
                return HumanWins;
            }
            else {
                return computerWins;
            }
        }
           
            
        
    }

}

